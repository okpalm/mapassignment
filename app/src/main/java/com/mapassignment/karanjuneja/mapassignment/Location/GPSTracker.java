package com.mapassignment.karanjuneja.mapassignment.Location;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;


import com.mapassignment.karanjuneja.mapassignment.MODEL.SendLocation.Request_AddLocation;
import com.mapassignment.karanjuneja.mapassignment.R;
import com.mapassignment.karanjuneja.mapassignment.Retrofit.MapApplication;
import com.mapassignment.karanjuneja.mapassignment.Service.APIUtility;
import com.mapassignment.karanjuneja.mapassignment.UI.MapsActivity;
import com.mapassignment.karanjuneja.mapassignment.Utilities.CONSTANTS;
import com.mapassignment.karanjuneja.mapassignment.Utilities.CommonUtils;
import com.mapassignment.karanjuneja.mapassignment.Utilities.Singleton;

import java.util.ArrayList;

import static android.app.NotificationManager.IMPORTANCE_HIGH;

public class GPSTracker extends Service {

    boolean started = false;
    APIUtility apiUtility;
    Locator locator;

    private Handler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        apiUtility = MapApplication.getApiUtility();
        mHandler = new Handler();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action;
        boolean fromReceiver = false;
        fromReceiver = intent.getBooleanExtra("receiver", false);
        if (intent != null) {
            action = intent.getAction();

            switch (action) {
                case CONSTANTS.START:
                    if (!started || fromReceiver) {
                        locator = new Locator(GPSTracker.this);
                        locator.getLocation(Locator.Method.GPS, new Locator.Listener() {
                            @Override
                            public void onLocationFound(Location location, float bearing) {
                                SendLoc(location);
                            }

                            @Override
                            public void onLocationNotFound() {

                            }
                        });
                        sendOrioNotification();
                        started = true;
                    }

                    break;
                case CONSTANTS.STOP:
                    try {
                        locator.cancel();
                        stopSelf();
                        this.stopForeground(true);
                        started = false;
                    } catch (Exception e) {
                    }
                    break;
            }
        }
        return Service.START_STICKY;
    }


    public void SendLoc(Location locationToSend) {
        if (locationToSend != null) {
            ArrayList<Double> arrayList = new ArrayList<>();

            arrayList.add(locationToSend.getLatitude());
            arrayList.add(locationToSend.getLongitude());
            Request_AddLocation request = new Request_AddLocation();
            request.setName(Singleton.getInstance().getId());
            request.setTime(CommonUtils.getCurrentTimeStamp());
            request.setLoc(arrayList);

            apiUtility.SendLocation(getApplicationContext(), false, request, new APIUtility.APIResponseListener<String>() {
                @Override
                public void onReceiveResponse(String response) {

                }
            });
            LogNotification(locationToSend);
        }

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private Notification sendNotification() throws Exception {


        Intent intent = new Intent(this, MapsActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        Notification n = new Notification.Builder(this)
                .setContentTitle(getResources().getText(R.string.app_name))
                .setContentText(getResources().getText(R.string.app_name))
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();
        return n;
    }


    private void sendOrioNotification() {
        // startForeground(10110, sendOrioNotification());
        CreateChannel();
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "11");
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();

        Intent notificationIntent = new Intent();
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notification.contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
        startForeground(10110, notification);

    }

    private void LogNotification(Location location) {
        CreateChannel();
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "11");
        Notification notification = notificationBuilder.setOngoing(false)
                .setContentTitle(String.valueOf(location.getLatitude()) + "  |  " + String.valueOf(location.getLongitude()))
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setSmallIcon(R.drawable.android_logo)
                .setCategory(Notification.CATEGORY_SOCIAL)
                .build();

        Intent notificationIntent = new Intent();
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notification.contentIntent = PendingIntent.getActivity(getApplicationContext(), 1106, notificationIntent, 0);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(1122, notification);

    }

    void CreateChannel() {
        String CHANNEL_ONE_ID = "11";
        String CHANNEL_ONE_NAME = "LOCATION";
        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ONE_ID,
                    CHANNEL_ONE_NAME, IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(notificationChannel);
        }


    }

}