package com.mapassignment.karanjuneja.mapassignment.UI;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mapassignment.karanjuneja.mapassignment.Adapters.WeatherAdapter;
import com.mapassignment.karanjuneja.mapassignment.Location.GPSTracker;
import com.mapassignment.karanjuneja.mapassignment.MODEL.GetLocation.LocationResponse;
import com.mapassignment.karanjuneja.mapassignment.R;
import com.mapassignment.karanjuneja.mapassignment.Retrofit.MapApplication;
import com.mapassignment.karanjuneja.mapassignment.Service.APIUtility;
import com.mapassignment.karanjuneja.mapassignment.Utilities.CONSTANTS;
import com.mapassignment.karanjuneja.mapassignment.Utilities.CommonUtils;
import com.mapassignment.karanjuneja.mapassignment.Utilities.Singleton;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    APIUtility apiUtility;

    Marker marker;

    Handler handler = new Handler();

    RecyclerView rv_weather;

    WeatherAdapter weatherAdapter = new WeatherAdapter();

    boolean isShow = true;

    AlertDialog alert;
    private BroadcastReceiver location = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra("case", 5)) {
                case 0:
                    if (isShow)
                        alert.dismiss();
                    break;
                case 1:
                    if (isShow)
                        alert.show();
                    break;
                default:
                    if (isShow)
                        alert.show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        apiUtility = MapApplication.getApiUtility();
        initViews();
        setupAlert();
        checkLocationPermission();

    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapStyle(new MapStyleOptions(CONSTANTS.myMapStyle));
        handler.post(getLoc);


    }

    Runnable getLoc = new Runnable() {
        @Override
        public void run() {
            getlocation();
            handler.postDelayed(this, 30000);
        }
    };

    void getlocation() {
        apiUtility.GetLocation(getApplicationContext(), false, new APIUtility.APIResponseListener<List<LocationResponse>>() {
            @Override
            public void onReceiveResponse(List<LocationResponse> response) {
                try {
                    if (response != null) {
                        LatLng loc = new LatLng(response.get(0).getLoc().get(0), response.get(0).getLoc().get(1));
                        mMap.clear();
                        marker = mMap.addMarker(new MarkerOptions()
                                .position(loc).title("Your Location")
                                .anchor(0.5f, 0.5f).flat(true).zIndex(1.0f));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15));
                    }
                } catch (Exception e) {
                }
            }
        });
    }


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Access")
                        .setMessage("Please provide Location permission to continue...")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        CONSTANTS.MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        CONSTANTS.MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            StartTracking();
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CONSTANTS.MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   StartTracking();
                } else {
                    finish();
                }
                return;
            }

        }
    }

    void initViews() {
        rv_weather = findViewById(R.id.rv_weather);
        rv_weather.setLayoutManager(new LinearLayoutManager(MapsActivity.this));
        rv_weather.setAdapter(weatherAdapter);
        weatherAdapter.refreshList(Singleton.getInstance().getWeather());
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(location, new IntentFilter("location"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //handler.removeCallbacks(getLoc);
        /*Intent intent = new Intent(MapsActivity.this, GPSTracker.class);
        intent.setAction(CONSTANTS.STOP);
        startService(intent);*/
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    void setupAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);
        alertDialog.setTitle("Location Settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert = alertDialog.create();
    }


    @Override
    protected void onPause() {
        super.onPause();
        isShow = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isShow = true;
    }


    void StartTracking() {
        Intent intent = new Intent(MapsActivity.this, GPSTracker.class);
        intent.setAction(CONSTANTS.START);
        if (CommonUtils.hasOreo())
            startForegroundService(intent);
        else
            startService(intent);
    }

    private void addAutoStartup() {

        try {
            Intent intent = new Intent();
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }

            List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if  (list.size() > 0) {
                startActivity(intent);
            }
        } catch (Exception e) {
            Log.e("exc" , String.valueOf(e));
        }
    }
}
