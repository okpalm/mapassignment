package com.mapassignment.karanjuneja.mapassignment.MODEL.Weather;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WeatherResponse {

    @SerializedName("coord")
    Coord coord;

    @SerializedName("sys")
    Sys sys;

    @SerializedName("weather")
    List<Weather> weather = new ArrayList<>();

    @SerializedName("main")
    Main main;

    @SerializedName("wind")
    wind wind;

    @SerializedName("name")
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.wind getWind() {
        return wind;
    }

    public void setWind(com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.wind wind) {
        this.wind = wind;
    }
}
