package com.mapassignment.karanjuneja.mapassignment.MODEL.SendLocation;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Request_AddLocation {

    @SerializedName("name")
    String name;

    @SerializedName("loc")
    ArrayList<Double> loc = new ArrayList<>();

    @SerializedName("time")
    String time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Double> getLoc() {
        return loc;
    }

    public void setLoc(ArrayList<Double> loc) {
        this.loc = loc;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
