package com.mapassignment.karanjuneja.mapassignment.Location;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.mapassignment.karanjuneja.mapassignment.UI.MapsActivity;
import com.mapassignment.karanjuneja.mapassignment.Utilities.CONSTANTS;
import com.mapassignment.karanjuneja.mapassignment.Utilities.CommonUtils;

import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class RestartWorker extends Worker {

    public RestartWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        restartLocationService();
        return Result.success();
    }

    void restartLocationService(){
        Intent intent = new Intent(getApplicationContext(), GPSTracker.class);
        intent.setAction(CONSTANTS.START);
        if (CommonUtils.hasOreo())
            getApplicationContext().startForegroundService(intent);
        else
            getApplicationContext().startService(intent);
    }
}
