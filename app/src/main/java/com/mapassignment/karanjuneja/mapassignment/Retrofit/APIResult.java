package com.mapassignment.karanjuneja.mapassignment.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIResult<T> {
    @SerializedName("status") @Expose
    private String status;
    @SerializedName("type") @Expose private boolean type;
    @SerializedName("message") @Expose private String message;
    @SerializedName("message") @Expose public T data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
