package com.mapassignment.karanjuneja.mapassignment.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.WeatherResponse;
import com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.WeatherWrapper;
import com.mapassignment.karanjuneja.mapassignment.R;

import java.util.ArrayList;
import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.Holder>{

    List<WeatherResponse> weatherList = new ArrayList<>();


    public WeatherAdapter() {
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_weather, viewGroup, false);
        return new WeatherAdapter.Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.temp.setText(String.valueOf(weatherList.get(i).getMain().getTemp()) + "°");
        holder.line1.setText(weatherList.get(i).getWeather().get(0).getDescription() + " | " + weatherList.get(i).getName());
        holder.line2.setText("Min Temp: " + String.valueOf(weatherList.get(i).getMain().getTemp_min() + "°" +
        " | " + "Max Temp: " + String.valueOf(weatherList.get(i).getMain().getTemp_max() + "°")));

    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView temp, line1, line2;
        public Holder(View view) {
            super(view);
            temp = view.findViewById(R.id.temp);
            line1 = view.findViewById(R.id.line1);
            line2 = view.findViewById(R.id.line2);
        }
    }

    public void refreshList(WeatherWrapper wrapper){
        weatherList = wrapper.getResponse();
        notifyDataSetChanged();
    }


}
