package com.mapassignment.karanjuneja.mapassignment.Location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.mapassignment.karanjuneja.mapassignment.Utilities.CONSTANTS;
import com.mapassignment.karanjuneja.mapassignment.Utilities.CommonUtils;

public class GpsLocationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            Toast.makeText(context, "in android.location.PROVIDERS_CHANGED",
                    Toast.LENGTH_SHORT).show();
            Intent pushIntent = new Intent(context, GPSTracker.class);
            pushIntent.setAction(CONSTANTS.START);
            pushIntent.putExtra("receiver",true);
            if (CommonUtils.hasOreo())
                context.startForegroundService(pushIntent);
            else
                context.startService(pushIntent);
        }
    }
}