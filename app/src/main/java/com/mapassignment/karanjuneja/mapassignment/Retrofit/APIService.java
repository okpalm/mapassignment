package com.mapassignment.karanjuneja.mapassignment.Retrofit;

import com.mapassignment.karanjuneja.mapassignment.MODEL.GetInterval.IntervalResponse;
import com.mapassignment.karanjuneja.mapassignment.MODEL.GetLocation.LocationResponse;
import com.mapassignment.karanjuneja.mapassignment.MODEL.SendLocation.Request_AddLocation;
import com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.WeatherResponse;
import com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.WeatherWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIService {
    @POST("latest")
    Call<String> SendLocation(@Body Request_AddLocation addLocation);

    @GET()
    Call<List<LocationResponse>> Getlocation(@Url String url);

    @GET()
    Call<WeatherWrapper> getWeather(@Url String url);

    @GET("latest/interval/test")
    Call<IntervalResponse> GetInterval();
}