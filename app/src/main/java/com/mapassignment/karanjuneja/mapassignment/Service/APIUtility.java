package com.mapassignment.karanjuneja.mapassignment.Service;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mapassignment.karanjuneja.mapassignment.MODEL.GetInterval.IntervalResponse;
import com.mapassignment.karanjuneja.mapassignment.MODEL.GetLocation.LocationResponse;
import com.mapassignment.karanjuneja.mapassignment.MODEL.SendLocation.Request_AddLocation;
import com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.WeatherWrapper;
import com.mapassignment.karanjuneja.mapassignment.Retrofit.APIService;
import com.mapassignment.karanjuneja.mapassignment.Retrofit.Helper.APIServiceGenerator;
import com.mapassignment.karanjuneja.mapassignment.Retrofit.Helper.ProcessDialog;
import com.mapassignment.karanjuneja.mapassignment.Utilities.CommonUtils;
import com.mapassignment.karanjuneja.mapassignment.Utilities.Singleton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Karan Juneja on 12/19/2017.
 */

public class APIUtility {
    private static final String TAG = "APIUtility";

    private APIService mApiService = null;
    private static final String BASE_URL = "https://96gw5cphgi.execute-api.ap-south-1.amazonaws.com/";



    public APIUtility() {
        APIServiceGenerator.setBaseUrl(BASE_URL);
        APIServiceGenerator.addHeader("Content-Type", "application/json");
        mApiService = APIServiceGenerator.createService(APIService.class);

    }


    public interface APIResponseListener<T> {
        void onReceiveResponse(T response);
        /* void onNetworkFailed();*/
    }

    public void showDialog(Context context, boolean isDialog) {
        if (isDialog) {
            ProcessDialog.start(context);
        }
    }

    public void dismissDialog(boolean isDialog) {
        if (isDialog) {
            ProcessDialog.dismiss();
        }
    }

    public void SendLocation(Context context, final boolean isDialog, final Request_AddLocation requestAddLocation, final  APIResponseListener<String> listener){
        CommonUtils.log(TAG, "onRequest: SendLocation" + new Gson().toJson(requestAddLocation));
        showDialog(context, isDialog);
        mApiService.SendLocation(requestAddLocation).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dismissDialog(isDialog);
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: SendLocation" + new Gson().toJson(response.body()));
                    listener.onReceiveResponse(response.body());
                } else {
                    listener.onReceiveResponse(null);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dismissDialog(isDialog);
                listener.onReceiveResponse(null);
            }
        });

    }

    public void GetLocation(Context context, final boolean isDialog, final APIResponseListener<List<LocationResponse>> listener){
        String url = "https://96gw5cphgi.execute-api.ap-south-1.amazonaws.com/latest/" + Singleton.getInstance().getId();
        mApiService.Getlocation(url).enqueue(new Callback<List<LocationResponse>>() {
            @Override
            public void onResponse(Call<List<LocationResponse>> call, Response<List<LocationResponse>> response) {
                dismissDialog(isDialog);
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: GetLocation" + new Gson().toJson(response.body()));
                    listener.onReceiveResponse(response.body());
                } else {
                    listener.onReceiveResponse(null);
                }
            }

            @Override
            public void onFailure(Call<List<LocationResponse>> call, Throwable t) {
                dismissDialog(isDialog);
                listener.onReceiveResponse(null);
            }
        });
    }

    public void GetWeather(Context context, final boolean isDialog, final APIResponseListener<WeatherWrapper> listener){
        mApiService.getWeather("http://api.openweathermap.org/data/2.5/group?id=1273294,1275339,1277333&units=metric&APPID=7bfd6da4a1505445348c3541c21294ee").enqueue(new Callback<WeatherWrapper>() {
            @Override
            public void onResponse(Call<WeatherWrapper> call, Response<WeatherWrapper> response) {
                dismissDialog(isDialog);
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: GetLocation" + new Gson().toJson(response.body()));
                    listener.onReceiveResponse(response.body());
                } else {
                    listener.onReceiveResponse(null);
                }
            }

            @Override
            public void onFailure(Call<WeatherWrapper> call, Throwable t) {
                dismissDialog(isDialog);
                listener.onReceiveResponse(null);
            }
        });
    }

    public void GetInterval(Context context, final boolean isDialog, final APIResponseListener<IntervalResponse> listener){
        mApiService.GetInterval().enqueue(new Callback<IntervalResponse>() {
            @Override
            public void onResponse(Call<IntervalResponse> call, Response<IntervalResponse> response) {
                dismissDialog(isDialog);
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: GetInterval" + new Gson().toJson(response.body()));
                    listener.onReceiveResponse(response.body());
                } else {
                    listener.onReceiveResponse(null);
                }
            }

            @Override
            public void onFailure(Call<IntervalResponse> call, Throwable t) {
                dismissDialog(isDialog);
                listener.onReceiveResponse(null);
            }
        });
    }
}