package com.mapassignment.karanjuneja.mapassignment.Location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.concurrent.TimeUnit;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class AutoStart extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
        {
            PeriodicWorkRequest.Builder serviceCheck =
                    new PeriodicWorkRequest.Builder(RestartWorker.class,30 ,
                            TimeUnit.MINUTES);

            PeriodicWorkRequest service = serviceCheck.build();
            WorkManager.getInstance().enqueue(service);

        }
    }
}