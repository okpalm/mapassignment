package com.mapassignment.karanjuneja.mapassignment.Utilities;

import com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.WeatherWrapper;

import java.util.ArrayList;
import java.util.List;

public class Singleton   {
    private static Singleton instance;

    WeatherWrapper weather;

    String id;

    int INTERVAL = 5000;

    public static Singleton getInstance() {
        // Return the instance
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getINTERVAL() {
        return INTERVAL;
    }

    public void setINTERVAL(int INTERVAL) {
        this.INTERVAL = INTERVAL * 60000;
    }

    public static void setInstance(Singleton instance) {
        Singleton.instance = instance;
    }

    public WeatherWrapper getWeather() {
        return weather;
    }

    public void setWeather(WeatherWrapper weather) {
        this.weather = weather;
    }
}
