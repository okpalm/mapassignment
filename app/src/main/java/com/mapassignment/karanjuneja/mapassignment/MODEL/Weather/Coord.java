package com.mapassignment.karanjuneja.mapassignment.MODEL.Weather;

import com.google.gson.annotations.SerializedName;

public class Coord {

    @SerializedName("lon")
    double lon;

    @SerializedName("lat")
    double lat;

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
