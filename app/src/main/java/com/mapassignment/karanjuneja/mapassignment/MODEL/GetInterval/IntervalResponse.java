package com.mapassignment.karanjuneja.mapassignment.MODEL.GetInterval;

import com.google.gson.annotations.SerializedName;

public class IntervalResponse {

    @SerializedName("interval")
    int interval;

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
