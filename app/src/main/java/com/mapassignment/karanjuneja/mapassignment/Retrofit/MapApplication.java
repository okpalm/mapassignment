package com.mapassignment.karanjuneja.mapassignment.Retrofit;

import android.app.Application;
import android.content.Context;

import com.mapassignment.karanjuneja.mapassignment.R;
import com.mapassignment.karanjuneja.mapassignment.Service.APIUtility;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MapApplication extends Application {

    private static APIUtility apiUtility;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        //Log.d("ALIEN","APPLICATION");
        super.onCreate();
        apiUtility = new APIUtility();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat_Light.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public static APIUtility getApiUtility(){
        return apiUtility;
    }
}
