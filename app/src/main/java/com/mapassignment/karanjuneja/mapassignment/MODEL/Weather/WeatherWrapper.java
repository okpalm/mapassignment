package com.mapassignment.karanjuneja.mapassignment.MODEL.Weather;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WeatherWrapper {

    @SerializedName("list")
    List<WeatherResponse> response = new ArrayList<>();

    public List<WeatherResponse> getResponse() {
        return response;
    }

    public void setResponse(List<WeatherResponse> response) {
        this.response = response;
    }
}
