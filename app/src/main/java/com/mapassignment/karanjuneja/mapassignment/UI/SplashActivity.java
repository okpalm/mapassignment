package com.mapassignment.karanjuneja.mapassignment.UI;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mapassignment.karanjuneja.mapassignment.MODEL.GetInterval.IntervalResponse;
import com.mapassignment.karanjuneja.mapassignment.MODEL.Weather.WeatherWrapper;
import com.mapassignment.karanjuneja.mapassignment.R;
import com.mapassignment.karanjuneja.mapassignment.Retrofit.MapApplication;
import com.mapassignment.karanjuneja.mapassignment.Service.APIUtility;
import com.mapassignment.karanjuneja.mapassignment.Utilities.Singleton;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final APIUtility apiUtility = MapApplication.getApiUtility();
        final String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        apiUtility.GetWeather(SplashActivity.this, false, new APIUtility.APIResponseListener<WeatherWrapper>() {
            @Override
            public void onReceiveResponse(WeatherWrapper response) {
                Singleton.getInstance().setWeather(response);
                Singleton.getInstance().setId(android_id);

               apiUtility.GetInterval(SplashActivity.this, false, new APIUtility.APIResponseListener<IntervalResponse>() {
                   @Override
                   public void onReceiveResponse(IntervalResponse response) {
                       if (response != null){
                           //Singleton.getInstance().setINTERVAL(response.getInterval());
                           Toast.makeText(getApplicationContext(),"Interval: "+ String.valueOf(response.getInterval()) + " Minutes",Toast.LENGTH_LONG).show();
                       }

                       new Thread()
                       {
                           public void run()
                           {
                               try {
                                   sleep(3000);
                                   startActivity(new Intent(SplashActivity.this, MapsActivity.class));
                                   finish();


                               } catch (InterruptedException e) {
                                   e.printStackTrace();
                               }
                           }

                       }.start();
                   }
               });
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
