package com.mapassignment.karanjuneja.mapassignment.MODEL.GetLocation;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LocationResponse {

    @SerializedName("_id")
    String id;

    @SerializedName("name")
    String name;

    @SerializedName("loc")
    ArrayList<Double> loc = new ArrayList<>();

    @SerializedName("time")
    String time;

    @SerializedName("createdAt")
    String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Double> getLoc() {
        return loc;
    }

    public void setLoc(ArrayList<Double> loc) {
        this.loc = loc;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
